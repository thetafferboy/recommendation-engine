You can get pearson coefficient top matches with:

> recommendations.topMatches(recommendations.critics,'Mark',n=11)

Where 'Mark' is the name of the critic and n= number of comparison

You can get weighted film recommendations with:

>recommendations.getRecommendations(recommendaitons.critics,'Mark')

Where 'Mark' is the name of the critic you want recommendations for

You can use Pearson coefficient with:

> recommendations.sim_pearson(recommendations.critics,'Mark', 'Tom')


You can use Euclidean Distance Score with:

> recommendations.sim_distance(recommendations.critics,'Mark', 'Tom')